
variable "AWS_REGION" {
  default = "us-west-2"
}

variable "VPC_NETWORK" {
  default = "10.11.0.0/16"
}

variable "AZ_COUNT" {
  default = "2"
}

variable "APP_COUNT" {
  default = "2"
}

variable "APP_IMAGE" {
  default     = "696044742141.dkr.ecr.us-west-2.amazonaws.com/testapp"
}

variable "APP_VERSION" {
  default = "latest"
}
