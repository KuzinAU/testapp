
resource "aws_cloudwatch_log_group" "app" {
  name              = "/ecs/app"
  retention_in_days = 3
  tags {
    Name = "app"
  }
}