provider "aws" {
  region     = "${var.AWS_REGION}"
}

terraform {
  backend "s3" {
    bucket = "kuzinau-terraform"
    key    = "testapp/orbita.tfstate"
    region = "us-west-2"
  }
}