import os

from flask import Flask, request, Response, jsonify
import boto3

db = boto3.resource('dynamodb')

table = db.Table('person')

app = Flask(__name__)

@app.route('/', methods=['POST'])
def post():
    
    content = request.json
    table.put_item(
        Item={
              'name': content['name'],
              'birthday': content['birthday']})

    return Response(status=204)

@app.route('/', methods=['GET'])
def get():
    
    content = request.json

    row = table.get_item(
        Key={
             'name': content['name'],
            })
    return jsonify(row['Item'])

@app.route('/ping')
def ping():
    return('pong')

if __name__ == '__main__':
    app.run(host='0.0.0.0')